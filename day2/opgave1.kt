import java.io.File
fun main(args: Array<String>) {
    
   var sum2 = 0
   var sum3 = 0
   var lines = File("input").readLines()
  
   for(line in lines) {
      var chc = Array<Int>(26,{_ -> 0})
      for (ch in line) {
            chc[ch.toInt() - 'a'.toInt()]++
      }
      if (chc.contains(2)) sum2++
      if (chc.contains(3)) sum3++ 
    }
   println(sum2 * sum3)
}