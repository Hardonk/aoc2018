import java.io.File
fun main(args: Array<String>) {
    
  
   var lines = File("input").readLines()
  
   for(line1 in lines) {
     for(line2 in lines) {
      var diff = ""
        for (i in 0 .. line1.length -1) {
           if (line1.get(i) == line2.get(i)) diff += line1.get(i)
        }
        if (line1.length - diff.length == 1) {
            println(diff)
           return 
        }
     }
   }
  
}