import java.io.File
fun main(args: Array<String>) {
    
  println(File("input").readLines().map{line ->  line.toInt() }.sum())
  
}