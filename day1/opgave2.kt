import java.io.File
fun main(args: Array<String>) {
    
  var sum = 0
  var values = File("input").readLines().map{line -> line.toInt()}
  var answers = HashMap<Int,Int>()
  while(true) {
   for( value in values) {
        sum += value;

        if (answers.contains(sum)) {
           println(sum)
          return
        }
        answers.set(sum,sum)
   }
  }
}